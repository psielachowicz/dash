import { DiagramComponent } from './diagrams/diagram/diagram.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'diagrams/1', pathMatch: 'full'
  },
  {
    path: 'diagrams', component: DiagramComponent
  },
  {
    path: 'diagrams/:id', component: DiagramComponent
  },
  {
    path: '**', component: DiagramComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
