import { AddDashboardComponent } from './dialogs/add-dashboard/add-dashboard.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DiagramService } from './services/diagram.service';
import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public dashboardList$ = this.diagramService.getDashboards();

  constructor(private diagramService: DiagramService, private matSnackBar: MatSnackBar, public dialog: MatDialog) {
  }

  openAddDashboardDialog(): void {
    const dialogRef = this.dialog.open(AddDashboardComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.addDashboard(result);
      }
    });
  }

  addDashboard(title: string): void {
    this.diagramService.addNewDashboard(title);
    this.matSnackBar.open(`New dashboard ${title} added`, 'ok', {
      duration: 2000,
    });
  }
}
