import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { Dashboard } from '../interfaces/diagram.interface';

import records from '../../assets/data/records.json';

@Injectable({
  providedIn: 'root'
})
export class DiagramService {
  public dashboardInitial: Dashboard[] = records.dashboards;
  private dashboardList = new BehaviorSubject<Dashboard[]>(this.dashboardInitial);

  getDashboards(): Observable<Dashboard[]> {
    return this.dashboardList.asObservable();
  }

  getDashboardById(id: number): Observable<Dashboard> {
    return of(this.dashboardList.value.find(el => el.id === id));
  }

  generateId(max = 10000): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  addNewDashboard(title: string) {
    this.dashboardList.next([...this.dashboardList.value, { id: this.generateId(), title, elements: [] }]);
  }

  updateDashboardElements(id: number, newElement) {
    const editIndex = this.dashboardList.value.findIndex(el => el.id === id);
    if (editIndex >= 0) {
      const editedDashboard = this.dashboardList.value;
      editedDashboard[editIndex].elements.push(newElement);
      this.dashboardList.next(editedDashboard);
    }
  }
}
