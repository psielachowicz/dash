export interface Dashboard {
  id: number;
  title: string;
  elements: DashboardElement[];
}

export interface DashboardElement {
  row: number;
  column: number;
  options: object;
  title: string;
}
