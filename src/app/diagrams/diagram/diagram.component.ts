import { AddDashboardElementComponent } from './../../dialogs/add-dashboard-element/add-dashboard-element.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiagramService } from 'src/app/services/diagram.service';
import * as Highcharts from 'highcharts';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Dashboard } from 'src/app/interfaces/diagram.interface';

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss']
})
export class DiagramComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private diagramService: DiagramService,
    private matDialog: MatDialog) { }

  public currentDashboard$: Observable<Dashboard>;
  public Highcharts = Highcharts;

  ngOnInit() {
   this.currentDashboard$ = this.route.paramMap
     .pipe(switchMap(params => this.diagramService.getDashboardById(parseInt(params.get('id'), 10))));
  }

  addElement(dashboardId: number): void {
    const addElemDialog = this.matDialog.open(AddDashboardElementComponent);
    addElemDialog.afterClosed().subscribe(result => {
      if (!!result) {
        this.diagramService.updateDashboardElements(dashboardId, result);
      }
    });
  }

}
