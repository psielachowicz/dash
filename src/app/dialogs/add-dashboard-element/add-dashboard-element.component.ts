import { Component, OnInit, Directive } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-add-dashboard-element',
  templateUrl: './add-dashboard-element.component.html',
  styleUrls: ['./add-dashboard-element.component.scss']
})
export class AddDashboardElementComponent implements OnInit {

  constructor(private fb: FormBuilder, private matSnackBar: MatSnackBar, public dialogRef: MatDialogRef<AddDashboardElementComponent>) { }

  public dashForm: FormGroup;

  ngOnInit() {

    this.dashForm = this.fb.group(
      {
        row: [1, [Validators.required, Validators.min(1), Validators.max(4)]],
        column: [1, [Validators.required, Validators.min(1), Validators.max(4)]],
        options: ['', Validators.required]
      }
    )
  }

  parseClose(): void {
    try {
      if  (this.dashForm.valid) {
        const chart = { ...this.dashForm.value, options: JSON.parse(this.dashForm.value.options) };
        this.dialogRef.close(chart);
      }
    } catch (err) {
      this.matSnackBar.open('Unable to parse insered JSON!');
    }
  }

  pasteExeplarValue() {
    const minimalOptions = {
      series: [{
        data: [1, 2, 3],
        type: 'line'
      }]
    };

    this.dashForm.controls['options'].setValue(JSON.stringify(minimalOptions));
  }
}
