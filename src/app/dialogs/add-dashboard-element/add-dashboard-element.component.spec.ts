import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDashboardElementComponent } from './add-dashboard-element.component';

describe('AddDashboardElementComponent', () => {
  let component: AddDashboardElementComponent;
  let fixture: ComponentFixture<AddDashboardElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDashboardElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDashboardElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
